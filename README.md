## Movielab 
    Author:
    Andrew Tec
    University of California, Irvine

## Overview

MovieLab is a modified version of PhotoLab, whose goal was to be able to apply various photo filters on a given .ppm photo. MovieLab's objective was to apply various image filters and movie effects to a movie file (.yuv) input by the user via the command line.

Debugging tools needed:

`valgrind`

### Features and Usage

PhotoLab

__>> Menu Based__

MovieLab

`MovieLab -i <file> -o <file> -f <framenum> -s <WIDTHxHEIGHT>`

Applies the following image filters:

    Aging (sepia effect)
        -aging
    Horizontal Flip
        -hflip
    Edge Detection
        -edge
    Zoom
        -zoom

Applies the following movie effects:

    Crop
        -crop <start-end>
    Fast Forward
        -fast <factor>
    Reverse
        -rvs

## Build Instructions

Start fresh with the following:

`make clean`

Create the executable with the following command:

`make MovieLab`

or

`make PhotoLab`

To automate testing and debugging

`make test`

To check for memory leakes

`make valgrind`

___
__! NOTE: I've been made aware people are unable to create the executable without the `yay.bin` file__

Deepest apologies.

## PhotoLab Outputs

Baseline Photo

![Baseline](previews/png/EH.png "Baseline Photo")

Negative Filter

![negative](previews/png/negative.png) 

Edge Detection

![edge](previews/png/edge.png)

Shuffle Filter

![shuffle](previews/png/shuffle.png)

Randomized Noise Filter

![noise](previews/png/noise.png)

Chromakey Filter (Color Filter)

![color](previews/png/colorfilter.png)

Horizontal Flip

![hflip](previews/png/hflip.png)

Rotate

![rotate](previews/png/rotate.png)

Vertical Mirroring

![vflip](previews/png/Vmirror.png)

Posterize

![poster](previews/png/posterize.png)

Overlay

![overlay](previews/png/overlay.png)

Resize (reduction)

![small](previews/png/smallresize.png)

Zoom / Enlarge

![zoom](previews/png/zoom.png)