/*********************************************************************/
/* Homework Assignment 5, for EECS 22, Fall 2016                     */
/*                                                                   */
/* Author: Guantao Liu                                               */
/* Date: 11/13/2016                                                  */
/*                                                                   */
/* MovieLab.c: source file for the main function                     */
/*                                                                   */
/* Please use this template for your HW5.                            */
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "DIPs.h"
#include "Movie.h"

/* Load one movie frame from the input file */
YUVIMAGE* LoadOneFrame(const char* fname, int n, unsigned int width, unsigned height);

/* Load the movie frames from the input file */
MOVIE *LoadMovie(const char *fname, int frameNum, unsigned int width, unsigned height);

/* Save the movie frames to the output file */
int SaveMovie(const char *fname, MOVIE *movie);

/* Print the command-line arguments usage of the program */
void PrintUsage();

int main(int argc, char *argv[]) {

	int x = 0, aging = 0, hflip = 0, edge = 0, crop = 0, fast = 0, reverse = 0, zoom = 0 /*,watermark = 0, spotlight = 0; */;
	unsigned int Width, Height, numberofFrames, crop_start, crop_end, fast_forward /* ,radius */;
	/***
	char *fwatermark = NULL;
	unsigned int fwatermarkLen = 0;
	***/
	char *fin = NULL;
	unsigned int finLen = 0;
	char *fout = NULL;
	unsigned int foutLen = 0;

	MOVIE *movie;
	IENTRY *entry;

	/* the while loop to check options in the command line */
	while(x < argc){
		/* the input file name */
		if (strcmp(argv[x], "-i") == 0){
			if (x < argc - 1){
				finLen = strlen(argv[x + 1]) + strlen(".yuv") + 1;
				fin = (char *) malloc(sizeof(char) * finLen);
				if (fin == NULL){
					printf("Error in memory allocation for the input file name!\n");
					free(fout);
					return 5;
				}
				strcpy(fin, argv[x + 1]);
				strcat(fin, ".yuv");
			} /*fi*/
			else{
				printf("Missing argument for the input file name!\n");
				free(fin);
				free(fout);
				return 5;
			} /*esle*/
			x += 2;
			continue;
		} /*fi*/

		/* the output file name */
		if (strcmp(argv[x], "-o") == 0){
			if (x < argc - 1){
				foutLen = strlen(argv[x + 1]) + strlen(".yuv") + 1;
				fout = (char *) malloc(sizeof(char) * foutLen);
				if (fout == NULL){
					printf("Error in memory allocation for the output file name!\n");
					free(fin);
					return 5;
				}
				strcpy(fout, argv[x + 1]);
				strcat(fout, ".yuv");
			} /*fi*/
			else{
				printf("Missing argument for the output file name!\n");
				free(fin);
				free(fout);
				return 5;
			} /*esle*/
			x += 2;
			continue;
		} /*fi*/

		/* determine number of frames */
		if (strcmp(argv[x], "-f") == 0){
			if (x < argc - 1){
				if (sscanf(argv[x + 1], "%d", &numberofFrames) != 1){
					printf("Information fed to -f is invalid…\n");
					PrintUsage();
					free(fin);
					free(fout);
				}

			} /*fi*/
			else{
				printf("NO FRAMENUMBER ARGUEMENT!\n");
				free(fin);
				free(fout);
				return 5;
			} /*esle*/
			x += 2;
			continue;
		} /*fi*/

		/* specify resolution and store width and height*/
		if (strcmp(argv[x], "-s") == 0){
			if (x < argc - 1){
				if (sscanf(argv[x + 1], "%ux%u", &Width, &Height) == 2){
				}
			} /*fi*/
			else{
				printf("NO RESOLUTION ARGUEMENT!\n");
				free(fin);
				free(fout);
				return 5;
			} /*esle*/
			x += 2;
			continue;
		} /*fi*/

		/* aging */
		if (strcmp(argv[x], "-aging") == 0){
			aging = 1;
			x++;
			continue;
		}

		/* hflip */
		if (strcmp(argv[x], "-hflip") == 0){
			hflip = 1;
			x++;
			continue;
		}

		/* edge */
		if ( strcmp(argv[x], "-edge") == 0){
			edge = 1;
			x++;
			continue;
		}

		/* crop */
		if ( strcmp(argv[x], "-crop") == 0){
			crop = 1;
			if (x < argc - 1){
				if (sscanf(argv[x + 1], "%u-%u", &crop_start, &crop_end) == 2){
				}
			} /*fi*/
			else{
				printf("NO CROP ARGUEMENT!\n");
				free(fin);
				free(fout);
				return 5;
			} /*esle*/
			x += 2;
			continue;
		}

		/* Fast Forward */
		if ( strcmp(argv[x], "-fast") == 0){
			fast = 1;
			if (x < argc - 1){
				if (sscanf(argv[x + 1], "%d", &fast_forward) == 1){
				}
			}
			else{
				printf("Missing fast forward factor information.\n");
				PrintUsage();
				free(fin);
				free(fout);
				return 5;
			}
			x += 2;
			continue;
		}/*fi*/
		
		/* reverse */
		if ( strcmp(argv[x], "-rvs") == 0){
			reverse = 1;
			x++;
			continue;
		}
		
		/* watermark goes here */
		/* spotlight goes here */
		
		/* zoom */
		if (strcmp(argv[x], "-zoom") == 0){
			zoom = 1;
			x++;
			continue;
		}

		/* the help option */
		if (strcmp(argv[x], "-h") == 0){
			PrintUsage();
			/* free(fwatermark) */
			free(fin);
			free(fout);
			return 0;
		} /*fi*/

		x++;
	} /*elihw*/
	
	/* create movie after ensuring all mandatory arguments are fed and are valid and optional arguments are checked */
    movie = LoadMovie(fin, numberofFrames, Width, Height);

	if (movie == NULL){
		free(fin);
		free(fout);
		return 5;
	}
	/*else{
		movie->Frames = CreateImageList();
		if (movie->Frames == NULL){
			free(fin);
			free(fout);
			free(movie);
			free(fwatermark)
		}
	}*/
	
	if(aging == 1){
	    YUV2RGBMovie(movie);
    	entry = movie->Frames->First;
    	assert(entry);

        while(entry){
            Aging(entry->RGBImage);
            entry = entry->Next;
        }
        RGB2YUVMovie(movie);
        printf("Operation Aging is done! \n");
    }

	if(hflip == 1){
        YUV2RGBMovie(movie);
    	entry = movie->Frames->First;
    	assert(entry);

        while(entry){
            HFlip(entry->RGBImage);
            entry = entry->Next;
        }
        RGB2YUVMovie(movie);
        printf("Operation HFlip is done! \n");
    }
    
    if(edge == 1){
        YUV2RGBMovie(movie);
    	entry = movie->Frames->First;
    	assert(entry);

    	while(entry){
            Edge(entry->RGBImage);
            entry = entry->Next;
        }
        RGB2YUVMovie(movie);
    	printf("Operation Edge is done! \n");
	}
	

    if(crop == 1){
        CropImageList(movie->Frames, crop_start, crop_end);
        printf("Operation Crop is done! \n");
    }

    if(fast == 1){
        FastImageList(movie->Frames, fast_forward);
        printf("Operation Fast Forward is done! \n");
    }
    
    if(reverse == 1){
        ReverseImageList(movie->Frames);
        printf("Operation Reverse is done! \n");
    }
	
    /* if watermark goes here */
	
	/* if spotlight goes here */
    
    if(zoom == 1){
        YUV2RGBMovie(movie);

    	entry = movie->Frames->First;
    	assert(entry);

        while(entry){
            Zoom(entry->RGBImage, ZOOM_FULL_SIZE);
            entry = entry->Next;
        }
        RGB2YUVMovie(movie);

        printf("Operation Zoom is done! \n");
    }
	
	if (fin == NULL){
		printf("No argument for the input file name!\n");
		PrintUsage();
		free(fin);
		free(fout);
		/* free(fwatermark); */
		return 5;
	}

	if (fout == NULL){
		printf("No argument for the output file name!\n");
		PrintUsage();
		free(fin);
		free(fout);
		/* free(fwatermark); */
		return 5;
	}
	
	/* fwatermark check */


   /* if(movie == NULL){
    		free (fin) ;
			free (fout) ;
			 free(fwatermark)
			DeleteMovie(movie);
			return 5 ;
		}*/
  /*  printf("number of frames %d \n", movie->Frames->Length);
    IENTRY *iterator = movie->Frames->First;
    int i = 0;
    while(iterator != NULL){
    	printf("%d : ", i);
    	printf("%d \n", iterator->YUVImage->U[2]);
    	iterator = iterator->Next;

    	i++;
    }
	printf("%d \n", movie->Frames->Last->YUVImage->Y[2]);*/

	SaveMovie(fout, movie);
	DeleteMovie(movie);
	movie = NULL;

	/* free(fwatermark) */
	free(fin);
	free(fout);

	return 0;
}

void PrintUsage()
{
	printf("\nUsage: MovieLab -i <file> -o <file> -f <framenum> -s <WIDTHxHEIGHT> [options]\n"
	       "Options:\n"
	       "-aging                Activate the aging filter on every movie frame\n"
	       "-hflip                Activate horizontal flip on every movie frame\n"
	       "-edge                 Activate the edge filter on every movie frame\n"
	       "-crop <start-end>     Crop the movie frames from <start> to <end>\n"
	       "-fast <factor>        Fast forward the movie by <factor>\n"
	       "-rvs                  Reverse the frame order of the input movie\n"
	       "-watermark <file>     Add a watermark from <file> to every movie frame\n"
	       "-spotlight <radius>   Spotlight a circle of <radius> on every movie frame\n"
	       "-zoom                 Zoom in and out the input movie\n"
	       "-h                    Display this usage information\n"
	      );
}

/* Load one movie frame from the input file */
YUVIMAGE* LoadOneFrame(const char* fname, int n, 
                       unsigned int width, unsigned height)
{
	FILE *file;
	unsigned int x, y;
	unsigned char c;
	YUVIMAGE* YUVimage;

	/* Check errors */
	assert(fname);
	assert(n >= 0);

	YUVimage = CreateYUVImage(width, height);
	if (YUVimage == NULL) {
		return NULL;
	}

	/* Open the input file */
	file = fopen(fname, "r");
	if (file == NULL) {
		DeleteYUVImage(YUVimage);
		return NULL;
	}

	/* Find the desired frame */
	fseek(file, 1.5 * n * width * height, SEEK_SET);

	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			c = fgetc(file);
			SetPixelY(YUVimage, x, y, c);
		} /*rof*/
	}

	for (y = 0; y < height; y += 2) {
		for (x = 0; x < width; x += 2) {
			c = fgetc(file);
			SetPixelU(YUVimage, x, y, c);
			SetPixelU(YUVimage, x + 1, y, c);
			SetPixelU(YUVimage, x, y + 1, c);
			SetPixelU(YUVimage, x + 1, y + 1, c);
		}
	}

	for (y = 0; y < height; y += 2) {
		for (x = 0; x < width; x += 2) {
			c = fgetc(file);
			SetPixelV(YUVimage, x, y, c);
			SetPixelV(YUVimage, x + 1, y, c);
			SetPixelV(YUVimage, x, y + 1, c);
			SetPixelV(YUVimage, x + 1, y + 1, c);
		}
	}

	/* Check errors */
	assert(ferror(file) == 0);

	/* Close the input file and return */
	fclose(file);
	file = NULL;
	return YUVimage;
}

/* Save the movie frames to the output file */
int SaveMovie(const char *fname, MOVIE *movie)
{
	int count; 
	int x, y;
	FILE *file;
	IENTRY *curr;

	/* Open the output file */
	file = fopen(fname, "w");
	if (file == NULL) {
		return 1;
	}
	 
	count = 0; 
	curr = movie->Frames->First;
	while (curr != NULL) {
		for (y = 0; y < curr->YUVImage->Height; y++) { 
			for (x = 0; x < curr->YUVImage->Width; x++) {
				fputc(GetPixelY(curr->YUVImage, x, y), file);
			}
		}

		for (y = 0; y < curr->YUVImage->Height; y += 2) { 
			for (x = 0; x < curr->YUVImage->Width; x += 2) {
				fputc(GetPixelU(curr->YUVImage, x, y), file);
			}
		}

		for (y = 0; y < curr->YUVImage->Height; y += 2) { 
			for (x = 0; x < curr->YUVImage->Width; x += 2) {
				fputc(GetPixelV(curr->YUVImage, x, y), file);
			}
		}
		
		curr = curr->Next;	 
		count++;
	}

	fclose(file);
	file = NULL;

	printf("The movie file %s has been written successfully!\n", fname);
	printf("%d frames are written to the file %s in total.\n", count, fname); 
	return 0;
}

MOVIE *LoadMovie(const char *fname, int frameNum, unsigned int width, unsigned height) {

	assert(fname);
	assert(frameNum >= 0);

	int frameCounter;
	MOVIE *film = CreateMovie();

	assert(film);

	for(frameCounter = 0; frameCounter < frameNum; frameCounter++){
		YUVIMAGE *filmFrame = LoadOneFrame("dive.yuv", frameCounter, width, height);
		AppendYUVImage(film->Frames, filmFrame);
	}

	return film;
}

/* EOF */
