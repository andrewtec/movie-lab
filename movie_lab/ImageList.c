#include <stdio.h> /* perror */
#include <stdlib.h> /* malloc */
#include <assert.h>	/* assertions */

#include "Movie.h"	/* Imagelist.h and Image.h */

/* Create a new image list */
ILIST *CreateImageList(void){

	ILIST *newimagelist = (ILIST *) malloc( sizeof( ILIST ));
	if (! newimagelist){
		perror("Out of memory! Aborting…");
		exit(10);
	}

	newimagelist->Length = 0;
	newimagelist->First = NULL;
	newimagelist->Last = NULL;

	return newimagelist;

}

/* Delete an image list (and all entries) */
void DeleteImageList(ILIST *list){

	IENTRY *e, *n;

	assert(list);

	e = list->First;
	while(e){
		n = e->Next;
		DeleteImage(e->RGBImage);
		DeleteYUVImage(e->YUVImage);
		free(e);
		e = n;
	}

	free(list);
}

/* Insert a RGB image to the image list at the end */
void AppendRGBImage(ILIST *list, IMAGE *RGBimage){

	IENTRY *entry = NULL;
	assert(list);
	assert(RGBimage);

	entry = (IENTRY *) malloc(sizeof(IENTRY));

	if(list->Last){
		entry->List = list;
		entry->Next = NULL;
		entry->Prev = list->Last;

		list->Last->Next = entry;
		list->Last = entry;
	}

	else{
		entry->List = list;
		entry->Next = NULL;
		entry->Prev = NULL;
		list->First = entry;
		list->Last = entry;
	}

	(list->Length)++;

}

/* Insert a YUV image to the image list at the end */
void AppendYUVImage(ILIST *list, YUVIMAGE *YUVimage){

	IENTRY *entry = NULL;
	assert(list);
	assert(YUVimage);

	entry = (IENTRY *) malloc(sizeof(IENTRY));

	if(list->Last){
		entry->List = list;
		entry->Next = NULL;
		entry->Prev = list->Last;

		list->Last->Next = entry;
		list->Last = entry;
	}

	else{
		entry->List = list;
		entry->Next = NULL;
		entry->Prev = NULL;
		list->First = entry;
		list->Last = entry;
	}

	(list->Length)++;
	entry->YUVImage = YUVimage;
}

/* Crop an image list */
void CropImageList(ILIST *list, unsigned int start, unsigned int end){

}

/* Fast forward an image list */
void FastImageList(ILIST *list, unsigned int factor){

}

/* Reverse an image list */
void ReverseImageList(ILIST *list){

}
