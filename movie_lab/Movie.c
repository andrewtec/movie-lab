#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "Movie.h"

int clip(int x);

/* Allocate the memory space for the movie and the memory space */
/* for the frame list. Return the pointer to the movie. */
MOVIE *CreateMovie(void){
	MOVIE *newMovie = (MOVIE *) malloc( sizeof(MOVIE));
	ILIST *newList = CreateImageList();

	if (! newMovie){
		perror("Out of memory! Aborting…");
		exit(10);
	}

	newMovie->Frames = newList;
	return newMovie;
}

/* Release the memory space for the frame list. */
/* Release the memory space for the movie. */
void DeleteMovie(MOVIE *movie){

	assert(movie);
	DeleteImageList(movie->Frames);
	free(movie);

}

/* Convert a YUV movie to a RGB movie */
void YUV2RGBMovie(MOVIE *movie){

	assert(movie);

	int Y, U, V, R, G, B, C, D, E, x, y;

	IENTRY *image = movie->Frames->First;
	assert(image);


	int WIDTH = image->YUVImage->Width;
	int HEIGHT = image->YUVImage->Height;

	while(image){
		IMAGE *toAdd = CreateImage(WIDTH, HEIGHT);
		image->RGBImage = toAdd;

		assert(image->YUVImage);
		for( y = 0; y < HEIGHT; y++){
			for( x = 0; x < WIDTH; x++){

				Y = GetPixelY(image->YUVImage, x, y);
				U = GetPixelU(image->YUVImage, x, y);
				V = GetPixelV(image->YUVImage, x, y);

				C = Y - 16;
				D = U - 128;
				E = V - 128;

				R = clip( ( (298 * C + 409 * E + 128) >> 8) ) ;
				G = clip( ( (298 * C - 100 * D - 208 * E + 128) >> 8) );
				B = clip( ( (298 * C + 516 * D + 128) >> 8) );

				SetPixelR(toAdd, x, y, R);
				SetPixelG(toAdd, x, y, G);
				SetPixelB(toAdd, x, y, B);
			}
		}


		/* image->YUVImage = NULL; */
		image = image->Next;
	}

	return;

}

/* Convert a RGB movie to a YUV movie */
void RGB2YUVMovie(MOVIE *movie){

	assert(movie);

	int Y, U, V, R, G, B, x, y;

	IENTRY *image = movie->Frames->First;
	assert(image);

	int WIDTH = image->RGBImage->Width;
	int HEIGHT = image->RGBImage->Height;

	while(image){

		for( y = 0; y < HEIGHT; y++){
			for( x = 0; x < WIDTH; x++){

				R = GetPixelR(image->RGBImage, x, y);
				G = GetPixelG(image->RGBImage, x, y);
				B = GetPixelB(image->RGBImage, x, y);

				Y = clip( ( (66 * R + 129 * G + 25 * B + 128) >> 8) + 16 ) ;
				U = clip( ( (-38 * R - 74 * G + 112 * B + 128) >> 8) + 128 );
				V = clip( ( (112 * R - 94 * G - 18 * B + 128) >> 8) + 128 );

				SetPixelY(image->YUVImage, x, y, Y);
				SetPixelU(image->YUVImage, x, y, U);
				SetPixelV(image->YUVImage, x, y, V);
			}
		}

		/* DeleteImage(image->RGBImage); */
		/* image->RGBImage = NULL; */
		image = image->Next;
	}

	return;
}

int clip(x){

	if(x < 0)
		x = 0;

	if(x > 255)
		x = 255;

	else if(x >= 0 && x <= 255)
		x = x;

	return x;
}
