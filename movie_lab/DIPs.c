/* DIPs.c: hw4, for EECS 22, 2016 fall
 * Author: Huan Chen
 * Date:   10/31/2016
 */
#include <assert.h> /* assert */
#include <string.h> /* memcpy */
#include <stdlib.h> /* abs */

#include "DIPs.h"
#include "FileIO.h"

IMAGE *Aging(IMAGE *image){
	assert(image);
	int x, y;
	unsigned char r, g, b;

	for (y = 0; y < image->Height; y++) {
		for (x = 0; x < image->Width; x++) {

			r = GetPixelR(image, x, y);
			g = GetPixelG(image, x, y);
			b = GetPixelB(image, x, y);

			SetPixelB(image, x, y, (r + g + b) / 5);
			SetPixelR(image, x, y, (unsigned char) (b * 1.6));
			SetPixelG(image, x, y, (unsigned char) (b * 1.6));
		}
	}

	return image;
}

IMAGE *Edge(IMAGE *image) {
	assert(image);
	IMAGE *tmpImage = CreateImage(image->Width, image->Height);
	assert(tmpImage);

	int x, y, m, n;
    memcpy(tmpImage->R, image->R, image->Width * image->Height * sizeof(unsigned char));
    memcpy(tmpImage->G, image->G, image->Width * image->Height * sizeof(unsigned char));
    memcpy(tmpImage->B, image->B, image->Width * image->Height * sizeof(unsigned char));
	for (y = 1; y < image->Height - 1; y++) {
		for (x = 1; x < image->Width - 1; x++) {
			int tmpR = 0;
			int tmpG = 0;
			int tmpB = 0;
			for (m = -1; m <= 1; m++) {
				for (n = -1; n <= 1; n++) {
					tmpR += (GetPixelR(tmpImage, x, y) - GetPixelR(tmpImage, x + n, y + m));
					tmpG += (GetPixelG(tmpImage, x, y) - GetPixelG(tmpImage, x + n, y + m));
					tmpB += (GetPixelB(tmpImage, x, y) - GetPixelB(tmpImage, x + n, y + m));
				}
			}
			SetPixelR(image, x, y, (tmpR > 255) ? 255 : (tmpR < 0) ? 0 : tmpR);
			SetPixelG(image, x, y, (tmpG > 255) ? 255 : (tmpG < 0) ? 0 : tmpG);
			SetPixelB(image, x, y, (tmpB > 255) ? 255 : (tmpB < 0) ? 0 : tmpB);
		}
	}
	for (y = 0; y < image->Height; y++) {
		x = 0;
		SetPixelR(image, x, y, 0);
		SetPixelG(image, x, y, 0);
		SetPixelB(image, x, y, 0);
		x = image->Width - 1;
		SetPixelR(image, x, y, 0);
		SetPixelG(image, x, y, 0);
		SetPixelB(image, x, y, 0);
	}
	for (x = 0; x < image->Width; x++) {
		y = 0;
		SetPixelR(image, x, y, 0);
		SetPixelG(image, x, y, 0);
		SetPixelB(image, x, y, 0);
		y = image->Height - 1;
		SetPixelR(image, x, y, 0);
		SetPixelG(image, x, y, 0);
		SetPixelB(image, x, y, 0);
	}
	DeleteImage(tmpImage);
	tmpImage = NULL;
    return image;
}

IMAGE *HFlip(IMAGE *image) {

	assert(image);
	int x, y;
	unsigned char r, g, b;

	for (y = 0; y < image->Height; y ++) {
		for (x = 0; x < image->Width / 2; x ++) {
			r = GetPixelR(image, image->Width - 1 - x, y);
			g = GetPixelG(image, image->Width - 1 - x, y);
			b = GetPixelB(image, image->Width - 1 - x, y);

			SetPixelR(image, image->Width - 1 - x, y, GetPixelR(image, x, y));
			SetPixelG(image, image->Width - 1 - x, y, GetPixelG(image, x, y));
			SetPixelB(image, image->Width - 1 - x, y, GetPixelB(image, x, y));

			SetPixelR(image, x, y, r);
			SetPixelG(image, x, y, g);
			SetPixelB(image, x, y, b);
		}
	}
    return image;
}

/* Add a watermark to an image */
IMAGE *Watermark(IMAGE *image, const IMAGE *watermark, unsigned int topLeftX, unsigned int topLeftY){
	return image;
}

IMAGE *Spotlight(IMAGE *image, int centerX, int centerY, unsigned int radius){
	return image;
}

IMAGE *Zoom(IMAGE *image, unsigned int percentage){
	return image;
}
