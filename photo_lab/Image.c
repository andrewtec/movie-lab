/********************************************/
/*	Image.c: by Andrew Tec					*/
/*	netID: atec								*/
/*	ID: 25381671							*/
/********************************************/

#include "Image.h"
#include <stdlib.h>
#include <assert.h>

/* Get the R intensity of pixel (x, y) in image */
unsigned char GetPixelR(const IMAGE *image, unsigned int x,  unsigned int y){

	assert(x <= (image -> Width));
	assert(y <= (image -> Height));
	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	return image -> R[x + y * (image -> Width)];
}

/* Get the G intensity of pixel (x, y) in image */
unsigned char GetPixelG(const IMAGE *image, unsigned int x,  unsigned int y){

	assert(x <= image -> Width);
	assert(y <= image -> Height);
	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	return image -> G[x + y * (image -> Width)];
}

/* Get the B intensity of pixel (x, y) in image */
unsigned char GetPixelB(const IMAGE *image, unsigned int x,  unsigned int y){

	assert(x <= image -> Width);
	assert(y <= image -> Height);
	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	return image -> B[x + y * (image -> Width)];
}

/* Set the R intensity of pixel (x, y) in image to r */
void SetPixelR(IMAGE *image, unsigned int x,  unsigned int y, unsigned char r){

	assert(x <= image -> Width);
	assert(y <= image -> Height);
	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	image->R[x + y * (image -> Width)] = r;
}

/* Set the G intensity of pixel (x, y) in image to g */
void SetPixelG(IMAGE *image, unsigned int x,  unsigned int y, unsigned char g){

	assert(x <= image -> Width);
	assert(y <= image -> Height);
	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	image->G[x + y * (image -> Width)] = g;
}

/* Set the B intensity of pixel (x, y) in image to b */
void SetPixelB(IMAGE *image, unsigned int x,  unsigned int y, unsigned char b){

	assert(x <= image -> Width);
	assert(y <= image -> Height);
	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	image->B[x + y * (image -> Width)] = b;
}

/* Allocate dynamic memory for the image structure and its R/G/B values */
/* Return the pointer to the image, or NULL in case of error */
IMAGE *CreateImage(unsigned int Width, unsigned int Height){

	IMAGE* imgptr;

	imgptr = (IMAGE *) malloc(sizeof(IMAGE));

	if( !imgptr )
		{ return NULL; }

	imgptr->Width = Width;
	imgptr->Height = Height;

	imgptr -> R = (unsigned char *) malloc(Width*Height*sizeof(unsigned char));
	if( !(imgptr->R) )
		{ return NULL; }

	imgptr -> G = (unsigned char *) malloc(Width*Height*sizeof(unsigned char));
	if( !(imgptr->G) )
		{ return NULL; }

	imgptr -> B = (unsigned char *) malloc(Width*Height*sizeof(unsigned char));
	if( !(imgptr->B) )
		{ return NULL; }

	return imgptr;
}

/* Free the memory for the R/G/B values and IMAGE structure */
void DeleteImage(IMAGE *image){

	assert(image);
	assert(image -> R);
	assert(image -> G);
	assert(image -> B);

	free(image -> R);
	free(image -> G);
	free(image -> B);
	image->R = NULL;
	image->G = NULL;
	image->B = NULL;
	free(image);
}
