/********************************************/
/*	DIPs.c: by Andrew Tec					*/
/*	netID: atec								*/
/*	ID: 25381671							*/
/*	Comments:								*/
/* 		All functions are derived from HW2	*/
/********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "Constants.h"
#include "DIPs.h"
#include "Image.h"

/* Negative */
IMAGE *Negative(IMAGE *image){

	int x, y;
	unsigned int HEIGHT, WIDTH;


	assert(image);


	HEIGHT = image->Height;
	WIDTH = image->Width;

	for( y = 0; y < HEIGHT; y ++ ) {
		for( x = 0; x < WIDTH; x ++ ) {
			SetPixelR( image, x, y,  (255 - GetPixelR( image, x, y)) );
			SetPixelG( image, x, y,  (255 - GetPixelG( image, x, y)) );
			SetPixelB( image, x, y,  (255 - GetPixelB( image, x, y)) );
		}
	}

	return image;
}

/* ColorFilter */
IMAGE *ColorFilter(IMAGE *image, int target_r, int target_g, int target_b,
  int threshold, int replace_r, int replace_g, int replace_b){

	assert(image);

	int x, y ;
	unsigned int HEIGHT, WIDTH;

	HEIGHT = image->Height;
	WIDTH = image->Width;


	for (y = 0; y < HEIGHT; y++) {
		for (x = 0; x < WIDTH; x++) {

			if (GetPixelR(image, x, y) >= (target_r - threshold) && GetPixelR(image, x, y) <= (target_r + threshold)) {
				if (GetPixelG(image, x, y) >= (target_g - threshold) && GetPixelG(image, x, y) <= (target_g + threshold)) {
					if (GetPixelB(image, x, y) >= (target_b - threshold) && GetPixelB(image, x, y) <= (target_b + threshold)) {

						SetPixelR( image, x, y,  replace_r );
						SetPixelG( image, x, y,  replace_g );
						SetPixelB( image, x, y,  replace_b );

					}
				}
			}

			else{
				SetPixelR(image, x, y, GetPixelR(image, x, y));
				SetPixelG(image, x, y, GetPixelG(image, x, y));
				SetPixelB(image, x, y, GetPixelB(image, x, y));
			}

		}
	}

	return image;
}

/* Edge */
IMAGE *Edge(IMAGE *image)
{
	assert(image);

	IMAGE* image_temp = NULL;

    int             x, y, m, n, a, b;
    int             tmpR = 0;
    int             tmpG = 0;
    int             tmpB = 0;
	unsigned int HEIGHT, WIDTH;

	HEIGHT = image->Height;
	WIDTH = image->Width;

	image_temp = CreateImage(WIDTH, HEIGHT);

	 for (y = 0; y < HEIGHT; y++){
	        for (x = 0; x < WIDTH; x++) {
	        	SetPixelR(image_temp, x, y, GetPixelR(image, x, y));
	        	SetPixelG(image_temp, x, y, GetPixelG(image, x, y));
	        	SetPixelB(image_temp, x, y, GetPixelB(image, x, y));
	        }
	    }

	    for (y = 1; y < HEIGHT - 1; y++){
	        for (x = 1; x < WIDTH - 1; x++){
	            for (n = -1; n <= 1; n++){
	                for (m = -1; m <= 1; m++) {
	                    a = x + m;
	                    b = y + n;
	                    if (a > WIDTH - 1)
	                        a = WIDTH - 1;
	                    if (a < 0)
	                        a = 0;
	                    if (b > HEIGHT - 1)
	                        b = HEIGHT - 1;
	                    if (b < 0)
	                        b = 0;
	                    if ((n==0)&&(m==0)) {
	                        tmpR += 8*GetPixelR(image_temp, a, b);
	                        tmpG += 8*GetPixelG(image_temp, a, b) ;
	                        tmpB += 8*GetPixelB(image_temp, a, b) ;
	                    }
	                    else {
	                        tmpR -= GetPixelR(image_temp, a, b) ;
	                        tmpG -= GetPixelG(image_temp, a, b) ;
	                        tmpB -= GetPixelB(image_temp, a, b) ;
	                    }
	                }
	            }
	            SetPixelR(image, x, y, (tmpR>MAX_PIXEL)? MAX_PIXEL: (tmpR<0)? 0: tmpR);
	            SetPixelG(image, x, y, (tmpG>MAX_PIXEL)? MAX_PIXEL: (tmpG<0)? 0: tmpG);
	            SetPixelB(image, x, y, (tmpB>MAX_PIXEL)? MAX_PIXEL: (tmpB<0)? 0: tmpB);
	            tmpR = tmpG = tmpB = 0;
	        }
	    }
	/*avoid edge border problems*/
	/*top and bottom*/
	for(x = 0; x < WIDTH; x++ ) {
		SetPixelR(image_temp, x, 0, 0);
		SetPixelG(image_temp, x, 0, 0);
		SetPixelB(image_temp, x, 0, 0);

		SetPixelR(image_temp, x, HEIGHT-1, 0);
		SetPixelG(image_temp, x, HEIGHT-1, 0);
		SetPixelB(image_temp, x, HEIGHT-1, 0);
	}

	/*left and right*/
	for(y = 0; y < HEIGHT; y++ ) {
		SetPixelR(image_temp, 0, y, 0);
		SetPixelG(image_temp, 0, y, 0);
		SetPixelB(image_temp, 0, y, 0);

		SetPixelR(image_temp, WIDTH-1, y, 0);
		SetPixelG(image_temp, WIDTH-1, y, 0);
		SetPixelB(image_temp, WIDTH-1, y, 0);
	}


	DeleteImage(image_temp);
	image_temp = NULL;

	return image;

}

/* HFlip */
IMAGE *HFlip(IMAGE *image)
{
	assert(image);

	int x, y;
	unsigned char r, g, b;
	unsigned int HEIGHT, WIDTH;

	HEIGHT = image->Height;
	WIDTH = image->Width;

	for (y = 0; y < HEIGHT; y ++) {
		for (x = 0; x < WIDTH / 2; x ++) {

			r = GetPixelR(image, WIDTH - 1 - x, y);
			g = GetPixelG(image, WIDTH - 1 - x, y);
			b = GetPixelB(image, WIDTH - 1 - x, y);

			SetPixelR(image, WIDTH - 1 - x, y, GetPixelR(image, x, y) );
			SetPixelG(image, WIDTH - 1 - x, y, GetPixelG(image, x, y) );
			SetPixelB(image, WIDTH - 1 - x, y, GetPixelB(image, x, y) );

			SetPixelR(image, x, y, r);
			SetPixelG(image, x, y, g);
			SetPixelB(image, x, y, b);
		}
	}

	return image;
}

/* VMirror */
IMAGE *Vmirror(IMAGE *image)
{
	assert(image);

	int x, y;
	unsigned int HEIGHT, WIDTH;

	HEIGHT = image->Height;
	WIDTH = image->Width;

	for (y = 0; y < HEIGHT / 2; y ++) {
		for (x = 0; x < WIDTH; x ++) {
			SetPixelR(image, x, HEIGHT - 1 - y, GetPixelR(image, x, y) );
			SetPixelG(image, x, HEIGHT - 1 - y, GetPixelG(image, x, y) );
			SetPixelB(image, x, HEIGHT - 1 - y, GetPixelB(image, x, y) );
		}
	}

	return image;
}


/* Zoom */
IMAGE *Zoom(IMAGE *image)
{
	assert(image);

	IMAGE *image_temp = NULL;

    int x, y;
	unsigned int HEIGHT, WIDTH;
	HEIGHT = image->Height;
	WIDTH = image->Width;

    const int X_OFFSET = WIDTH / 4;
    const int Y_OFFSET = HEIGHT / 4;


	image_temp = CreateImage(WIDTH, HEIGHT);

    for (y = 0; y < HEIGHT; y++) {
        for (x = 0; x < WIDTH; x++) {
        	SetPixelR(image_temp, x, y, GetPixelR(image, x, y) );
        	SetPixelG(image_temp, x, y, GetPixelG(image, x, y) );
        	SetPixelB(image_temp, x, y, GetPixelB(image, x, y) );
        }
    }

    for (y = 0; y < HEIGHT; y++) {
        for (x = 0; x < WIDTH; x++) {
        	SetPixelR(image, x, y, GetPixelR(image_temp, x / ZOOM_FACTOR + X_OFFSET, y / ZOOM_FACTOR + Y_OFFSET) );
        	SetPixelG(image, x, y, GetPixelG(image_temp, x / ZOOM_FACTOR + X_OFFSET, y / ZOOM_FACTOR + Y_OFFSET) );
        	SetPixelB(image, x, y, GetPixelB(image_temp, x / ZOOM_FACTOR + X_OFFSET, y / ZOOM_FACTOR + Y_OFFSET) );
        }
    }

	DeleteImage(image_temp);
	image_temp = NULL;
	return image;
}
