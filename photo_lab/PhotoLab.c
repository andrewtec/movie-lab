/********************************************/
/*	PhotoLab3: by Andrew Tec				*/
/*	netID: atec								*/
/*	ID: 25381671							*/
/*	comments:								*/
/* 		AutoTest() taken from Test.c		*/
/********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Constants.h"
#include "Advanced.h"
#include "FileIO.h"
#include "DIPs.h"
#include "Test.h"
#include "Image.h"

/* print a menu */
void PrintMenu();

int main() {

#ifdef DEBUG
	AutoTest();
#endif /* DEBUG */

#ifndef DEBUG

	int FileNotRead;
	IMAGE *image = NULL;
	IMAGE *ov_image = NULL;

	char fname[SLEN];
	int choice;
	int target_r;
	int target_g;
	int target_b;
	int threshold;
	int noisePercent;
	int posterR;
	int posterG;
	int posterB;
	int x_offset;
	int y_offset;
	int backR;
	int backG;
	int backB;
	int degrees;
	int cropWidth;
	int cropHeight;
	int rPercentage;
	int oRadius;
	int iRadius;
	double factor_r;
	double factor_g;
	double factor_b;

	FileNotRead = 1;
	PrintMenu();
	printf("please make your choice: ");
	scanf("%d", &choice);

	while (choice != 18) {
		switch (choice) {
		case 1:
			printf("Please input the file name to load: ");
			scanf("%s", fname);
			image = LoadImage(fname);
			if (image) FileNotRead = 0;
			break;
		case 2:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Please input the file name to save: ");
				scanf("%s", fname);
				SaveImage(fname, image);
			}
			break;
		case 3:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				Negative(image);
				printf("\"Negative\" operation is done!\n");
			}
			break;
		case 4:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Enter Red   component for the target color: ");
				scanf("%d", &target_r);
				printf("Enter Green component for the target color: ");
				scanf("%d", &target_g);
				printf("Enter Blue  component for the target color: ");
				scanf("%d", &target_b);
				printf("Enter threshold for the color difference: ");
				scanf("%d", &threshold);
				printf("Enter value for Red component in the target color: ");
				scanf("%lf", &factor_r);
				printf("Enter value for Green component in the target color: ");
				scanf("%lf", &factor_g);
				printf("Enter value for Blue  component in the target color: ");
				scanf("%lf", &factor_b);

				ColorFilter(image, target_r, target_g, target_b, threshold,
						factor_r, factor_g, factor_b);
				printf("\"Color Filter\" operation is done!\n");
			}
			break;
		case 5:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				Edge(image);
				printf("\"Edge\" operation is done!\n");
			}
			break;
		case 6:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				HFlip(image);
				printf("\"Horizontally Flip\" operation is done!\n");
			}
			break;
		case 7:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				Vmirror(image);
				printf("\"Vertically Mirror\" operation is done!\n");
			}
			break;
		case 8:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				Zoom(image);
				printf("\"Zoom\" operation is done!\n");
			}
			break;
		case 9:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Please input noise percentage: ");
				scanf("%d", &noisePercent);
				printf("Before noise\n");
				AddNoise(image, noisePercent);
				printf("\"AddNoise\" operation is done!\n");
			}
			break;
		case 10:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				Shuffle(image);
				printf("\"Shuffle\" operation is done!\n");
			}
			break;
		case 11:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf(
						"Enter the number of posterization bits for R channel (1 to 8): ");
				scanf("%d", &posterR);
				printf(
						"Enter the number of posterization bits for G channel (1 to 8): ");
				scanf("%d", &posterG);
				printf(
						"Enter the number of posterization bits for B channel (1 to 8): ");
				scanf("%d", &posterB);
				Posterize(image, posterR, posterG, posterB);
				printf("\"Posterize\" operation is done!\n");
			}
			break;

		case 12:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Please input X coordinate of the overlay image: ");
				scanf("%d", &x_offset);
				printf("Please input Y coordinate of the overlay image: ");
				scanf("%d", &y_offset);
				printf("Please input background R: ");
				scanf("%d", &backR);
				printf("Please input background G: ");
				scanf("%d", &backG);
				printf("Please input background B: ");
				scanf("%d", &backB);
				ov_image = LoadImage("balloon");
				Overlay(image, ov_image, x_offset, y_offset, backR, backG,
						backB);
				printf("\"Overlay\" operation is done!\n");
			}
			break;
		case 13:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Please input the rotate degree (0, 90, 180 or -90): ");
				scanf("%d", &degrees);
				Rotate(image, degrees);
				printf("\"Rotate\" operation is done!\n");
			}
			break;

		case 14:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Please enter the X offset value: ");
				scanf("%d", &x_offset);
				printf("Please enter the Y offset value: ");
				scanf("%d", &y_offset);
				printf("Please input the crop width: ");
				scanf("%d", &cropWidth);
				printf("Please input the crop height: ");
				scanf("%d", &cropHeight);
				Crop(image, x_offset, y_offset, cropWidth, cropHeight);
				printf("\"Crop\" operation is done!\n");
			}
			break;
		case 15:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf(
						"Please input the resizing percentage (integer between 1~500): ");
				scanf("%d", &rPercentage);
				Resize(image, rPercentage);
				printf("\"Resizing the image\" operation is done!\n");
			}
			break;
		case 16:
			if (FileNotRead != 0) {
				printf("No Image Read Yet !!\n");
			} else {
				printf("Please enter the X offset value: ");
				scanf("%d", &x_offset);
				printf("Please enter the Y offset value: ");
				scanf("%d", &y_offset);
				printf("Please input the outer radius: ");
				scanf("%d", &oRadius);
				printf("Please input the inner radius: ");
				scanf("%d", &iRadius);
				MetalCircle(image, x_offset, y_offset, oRadius, iRadius);
				printf("\"MetalCircle\" operation is done!\n");
			}
			break;

		case 17:
			AutoTest();
			FileNotRead = 0;
			break;

		default:
			printf("Invalid selection!\n");
			break;
		}

		PrintMenu();
		printf("please make your choice: ");
		scanf("%d", &choice);
	}

#endif /* DEBUG */

	return 0;
}

/* Menu */
void PrintMenu() {
printf("\n--------------------------------\n");
printf(" 1:  Load a PPM image\n");
printf(" 2:  Save an image in PPM and JPEG format\n");
printf(" 3:  Make a negative of an image\n");
printf(" 4:  Color filter an image\n");
printf(" 5:  Sketch the edge of an image\n");
printf(" 6:  Flip an image horizontally\n");
printf(" 7:  Mirror an image vertically\n");
printf(" 8:  Zoom an image\n");
printf(" 9:  Add noise to an image\n");
printf("10:  Shuffle an image\n");
printf("11:  Posterize an image\n");
printf("12:  Overlay\n");
printf("13:  Rotate clockwise\n");
printf("14:  Crop\n");
printf("15:  Resize\n");
printf("16:  MetalCircle\n");
printf("17:  Test all functions\n");
printf("18:  Exit\n");
}
