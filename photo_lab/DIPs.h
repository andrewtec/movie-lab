/*** Assignment 4 DIP function declarations ***/
#ifndef DIPS_H
#define DIPS_H

#include "Image.h"
#include "Constants.h"

/* DIPs.h */
IMAGE *Negative(IMAGE *image);
IMAGE *ColorFilter(IMAGE *image, int target_r, int target_g, int target_b,
  int threshold, int replace_r, int replace_g, int replace_b);
IMAGE *Edge(IMAGE *image);
IMAGE *HFlip(IMAGE *image);
IMAGE *Vmirror(IMAGE *image);
IMAGE *Zoom(IMAGE *image);

#endif /* DIPS_H */
